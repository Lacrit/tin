const operations = {
    '/mult': {sign: "*", action: (a, b) => a * b},
    '/divide': {sign: "/", action: (a, b) => a / b},
    '/add': {sign: "+", action: (a, b) => a + b},
    '/subtract': {sign: "-", action: (a, b) => a - b}
};

const http = require('http');
const url = require("url");
const reg = /^\d+$/;
//create a server object:
http.createServer((req, res) =>{
    try {
    const qs = url.parse(req.url, true);
    const operator = qs.pathname;
    const a = qs.query.a;
    const b = qs.query.b;
    if (!reg.test(a) || !reg.test(b)) { 
        res.statusCode = 400;
        res.end('A or B is not a number');
    }
    res.end(
    '<h1>' + `${a} ${operations[operator].sign} ${b}` + ` = ${operations[operator].action(parseInt(a), parseInt(b))} ` + '</h1>'
    );
    } catch(error) { 
        res.statusCode = 500;
        res.end("Server error");
    }

}).listen(8000); //the server object listens on port 3000