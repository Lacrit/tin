import React from 'react';
import ReactDOM from 'react-dom';
import './screens/App';
import App from './screens/App';

ReactDOM.render(<App />, document.getElementById('root'));
