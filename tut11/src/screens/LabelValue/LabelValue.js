import React from 'react';

const LabelValue = function(props){
    return <div>{props.data.label} : {props.data.value}</div>
};

export default LabelValue;