import React, {  Component } from 'react';
import Text from '../Text/Text';
import LabelValue from '../LabelValue/LabelValue';

class Form extends Component {
    state = {
        description: "",
        hours: 0,
        email: "",
        messageForDescription: "Description is too short",
        messageForHours: "Number of hours is too big",
        messageForEmail: "Email is wrong",
        isDescValid: false,
        isHoursValid: false,
        isEmailValid: false
    }
    
    descriptionHandleChange = (val) => { 
        if (val.length < 10)
            this.setState({ 
                description: val,
                messageForDescription: "Description is too short",
                isDescValid: false
            });
        else
            this.setState({ 
                description: val,
                messageForDescription: "",
                isDescValid: true 
            });
    }

    hoursHandleChange = (val) => { 
        if (val > 24 || val < 0) 
            this.setState({ 
                hours: val,
                messageForHours: "Number of hours is invalid",
                isHoursValid: false
            });
        else
            this.setState({ hours: val , messageForHours: "" , isHoursValid: true});
    }

    emailHandleChange = (val) => {
        const emailValid = val.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        if (!emailValid) 
            this.setState({ 
                email: val ,
                messageForEmail: "Email is wrong",
                isEmailValid: false
            });
        else
            this.setState({
                email: val,
                messageForEmail: "",
                isEmailValid: true 
            });
    }

    render() {
        return (
        <div>
            <form>
                <label htmlFor='description'>Description: </label>
                <input type="text" name="description" id="description" value={this.state.description}
                    onChange={event => this.descriptionHandleChange(event.target.value)}></input> 
                <Text class="invalid" text={this.state.messageForDescription}></Text><br/>
                <label htmlFor='hours'>Hours: </label>
                <input type="number" name="hours" id='hours' value={this.state.hours}
                    onChange={event => this.hoursHandleChange(event.target.value)}></input>
                <Text class="invalid"  text={this.state.messageForHours}></Text><br/>
                <label htmlFor='email'>Email: </label>
                <input type="email" name='email' id='email' value={this.state.email}
                    onChange={event => this.emailHandleChange(event.target.value)}></input>
                <Text class="invalid"  text={this.state.messageForEmail }></Text><br/>


                <LabelValue 
                    class={this.state.isDescValid.toString()} 
                    data={{
                        label: "Is description field valid", 
                        value: this.state.isDescValid.toString() 
                    }}>
                </LabelValue>
                <LabelValue 
                    class={this.state.isHoursValid.toString()} 
                    data={{
                        label: "Is hours field valid", 
                        value: this.state.isHoursValid.toString() 
                    }}>
                </LabelValue>
                <LabelValue 
                    class={this.state.isEmailValid.toString()} 
                    data={{
                        label: "Is email field valid", 
                        value: this.state.isEmailValid.toString() 
                    }}>
                </LabelValue>
            </form>
        </div>
        );
    }
}

export default Form;