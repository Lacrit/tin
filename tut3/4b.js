 memoize = (fn) => {
    const cache = {}
    return (...args) => {
      if (cache[args]){
        return cache[args];
      }
      newCall = fn.apply(null, args);
      cache[args] = newCall;
      return newCall;
    }
}

fib = (n) => {
    if (n < 2) {
      return n;
    }
    return fastFib(n - 1) + fastFib(n - 2);
}

const fastFib = memoize(fib);

let num = 25;
console.log(`\x1b[36mFibbonaci recursion optimized | ${num}:\x1b[0m`, fib(num));