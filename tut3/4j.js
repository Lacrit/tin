const binarySearch = (array, value) => {
    const middle = Math.floor(array.length/2);
    const middleElement = array[middle];

    if (array.length === 1) return (array[0] === value) ? true : false;
    //left side 
    if (middleElement > value) return binarySearch(array.slice(0, middle), value);
    //right side
    else if (middleElement < value) return binarySearch(array.slice(middle, array.length), value);
    //equals
    else return true;
};

let val = 8;
let arr = [1,2,3,4,5,6,7,8,9,10];
console.log(`\x1b[36mBinary search for ${arr} | element ${val}:\x1b[0m`, binarySearch(arr, val));