palindrome = (str) => { 
    for (let i = 0; i < str.len/2; i++) {
      if (str[i] !== str[str.len - 1 - i]) 
          return false;
    }
    return true;
}

let str = 'amanaplanacanalpanama';
console.log(`\x1b[36mCheck palindrome | ${str}:\x1b[0m`, palindrome(str));