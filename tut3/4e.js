findLongestWord = (str) => {
  let arr = str.match(/\w[a-z]{0,}/gi);
  let result = arr[0];

  for(let i = 1 ; i < arr.length ; i++)
  {
    if(result.length < arr[i].length)
    {
        result = arr[i];
    } 
  }
  return result;
}

let str = 'Check is it woooooorks'
console.log(`\x1b[36mFind the longest word | ${str}:\x1b[0m`, findLongestWord(str));