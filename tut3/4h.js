sortNumber = (a,b) => {
    return a - b;
}

secondGreatestLowestNum = (arr) => { 
    arr.sort(sortNumber);
    return arr[1] + ', ' + arr[arr.length-2];
}

let arr = [2, -3 , 100, 1, 3, 30, 21];
console.log(`\x1b[36mSecond lowest and greatest elements in ${arr}:\x1b[0m`, secondGreatestLowestNum(arr));