isPrime = (num) => { 
    let prime = true;
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            prime = false;
            break;
        }
    }
    return prime && (num > 1);
}
    
let num = 13;
console.log( `\x1b[36misPrime ${num}: \x1b[0m`, isPrime(num),)