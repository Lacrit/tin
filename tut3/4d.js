alphabeticalOrder = (str) => { 
    return str.split('').sort().join('');
}

let str = 'webmaster';
console.log(`\x1b[36mAlphabetical order | ${str}:\x1b[0m`, alphabeticalOrder(str));