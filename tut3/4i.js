amountToCoins = (num, coins) => {
    return coins.reduce((res, coin) => {
        while(num - coin >= 0) {
        res.push(coin);
        num -= coin;
        }
    return res;
}, []);
}

let num = 46; 
let coins = [25, 10, 5, 2, 1];
console.log(`\x1b[36mAmount to coins ${num}:\x1b[0m`, amountToCoins(num, coins));