let factorialRec = (n) => { 
    return (n == 0) ? 1 : n * factorialRec(n - 1);
}

factorialIter = (n) => { 
    let res = 1;
    for ( let i=1; i<=n; i++ )
        res *= i;
    return res;
}

let num = 10;
console.log(
    `\x1b[36mFactorial of ${num} || recursion \x1b[0m`,
    factorialRec(num),
    '\x1b[33m|| iteration \x1b[0m',
    factorialIter(num));
