function Student(firstName, lastName, id, grades){
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
    this.grades = grades;

    Object.defineProperties(this, {
        'avgGrade': { 
            get: () => { 
                return this.grades.reduce((x, y) => x + y) / this.grades.length; } 
            },
        'fullName': { 
            get: () => {
                 return this.firstName + ' ' + this.lastName; 
            },
            set: (name) => {
                let words = name.split(' ');
                this.firstName = words[0] || '';
                this.lastName = words[1] || '';
            }
        }
    });

    this.getInfo = () => {
        console.log(this.firstName + ' ' + this.lastName + ' has average grade of ' + this.avgGrade);
    }
}

let student = new Student('Anna', 'Voitenkova', '15486', [4, 5, 5, 3]);

student.getInfo();

student.fullName = 'Dawid Bowie';

student.getInfo();
