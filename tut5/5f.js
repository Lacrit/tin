class Person{
    constructor(firstName, lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
    get fullName() {
        return this.firstName + ' ' + this.lastName;
    }

    set fullName(name){
        let words = name.split(' ');
        this.firstName = words[0] || '';
        this.lastName = words[1] || '';
    }
}


class Student extends Person{
    constructor(firstName, lastName, id, grades){
        super(firstName, lastName);
        this.id = id;
        this.grades = grades;
    }

    get avgGrade() {
        return this.grades.reduce((x, y) => x + y) / this.grades.length;
    }
}

let student = new Student('Anna', 'Voitenkova', '15486', [4, 5, 5, 3]);

console.log(student.firstName, student.lastName, "has average grade of", student.avgGrade)

student.fullName = 'Dawid Bowie';

console.log(student.firstName, student.lastName, "has average grade of", student.avgGrade)