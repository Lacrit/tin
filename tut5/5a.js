let dog_obj = {
    legs: 4,
    ears: 2,
    tail: 1,
    eyes: 2,
    voice: () => {
        return 'bark';
    },
    walk: () => {
        return 'top';
    },
    dance: () => {
        return 'shake';
    }
};

list_props = (obj) => {
    for(let prop in obj){
        console.log(prop + ' of type ' + typeof(obj[prop]));
    }
}

list_props(dog_obj);