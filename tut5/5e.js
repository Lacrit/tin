class Student {
    constructor(firstName, lastName, id, grades) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.grades = grades;
    }

    get avgGrade() {
        return this.grades.reduce((x, y) => x + y) / this.grades.length;
    }

    get fullName() {
        return this.firstName + ' ' + this.lastName;
    }

    set fullName(name){
        let words = name.split(' ');
        this.firstName = words[0] || '';
        this.lastName = words[1] || '';
    }
}

let student = new Student('Anna', 'Voitenkova', '15486', [4, 5, 5, 3]);

console.log(student.firstName, student.lastName, "has average grade of", student.avgGrade)

student.fullName = 'Dawid Bowie';

console.log(student.firstName, student.lastName, "has average grade of", student.avgGrade)