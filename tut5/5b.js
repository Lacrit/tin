function Student(firstName, lastName, id, grades){
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
    this.grades = grades;

    this.getInfo = () => {
        let avgGrade = this.grades.reduce((x, y) => x + y) / this.grades.length;
        console.log(this.firstName + ' ' + this.lastName + ' has average grade of ' + avgGrade);
    }
}

let student = new Student('Anna', 'Voitenkova', '15486', [4, 5, 5, 3]);

student.getInfo();