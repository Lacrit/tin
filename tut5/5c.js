let studentProto = {
    obligatoryCourses: ['TAK', 'PPJ', 'PRO', 'SAD', 'BYT']
}

function newStudent(proto, firstName, lastName, id){
    let obj = Object.create(proto);
    obj.firstName = firstName;
    obj.lastName = lastName;
    obj.id = id;
    return obj;
}

let student = newStudent(studentProto, 'Anna', 'Voitenkova', '15486');

console.log("Name:", student.firstName, student.lastName, "\ncourses:", student.obligatoryCourses);